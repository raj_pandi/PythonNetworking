#This code is contributed to stack overflow
#!/usr/bin/python

from scapy. all import *

def stopfilter(x):
    if x[IP].dst == 'src_ip': #src_ip
        return True
    else:
        return False

get = 'GET /index.html HTTP/1.1\n\n'
syn_ip  = IP(src='src_ip', dst='dst_ip')  #enter your ip's here
syn_syn = TCP(sport = 5558, dport=80, flags='S',seq = 1000)
syn_ack = sr1(syn_ip/syn_syn,verbose=0)

if syn_ack:
    temp = syn_ack.seq
    myack  = temp + 1
    ack_packet = TCP(sport=5558,dport=80,flags='A',seq=syn_ack.ack,ack=myack)
    send(syn_ip/ack_packet,verbose=0)
    payload_packet = TCP(sport=5558,dport=80,flags='A',seq=syn_ack.ack, ack=myack)
    p = syn_ip/payload_packet/get
    server_resp = sr1(p,verbose=0)
    a = sniff(iface = "eth9",filter = "tcp port 80",stop_filter=stopfilter)

    for packet in a:
        if packet.getlayer(Raw):
            l = packet.getlayer(Raw).load
            rawr=Raw(l)
            rawr.show()